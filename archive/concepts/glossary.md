---
layout: default
parent: Concepts
nav_order: 3
title: "Glossary"
date: 2020-06-26T07:51:53-04:00
---

The documents in this repository reference a specialized set of terms that have
evolved over the course of the development effort.

### Oughtomation

### Rule Taker (XA-RT)

A _rule taker_ is an application that pulls information from Interlibr in order to apply oughtomation to specific data using [the matching algorithm](/concepts/matching). This algorithm may be implemented by a software component offered by Xalgorithms. If so, when we refer to XA-RT, we generally are referring to these components.

### Rule Maker (XA-RM)

A _rule maker_ is an application creates or changes rules. Xalgorithms has implemented a web-based rule authoring IDE. When we refer to XA-RM, we are typically referring to this application.
